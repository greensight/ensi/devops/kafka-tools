all: build

.PHONY: build
build:
	docker build -f docker/Dockerfile -t kafka-tools:latest .

.PHONY: run
run:
	docker run --rm -it -v $$PWD:/wd -w /wd -u $$(id -u):$$(id -g) --network=host kafka-tools:latest bash