# Kafka tools

Скрипт для создания топиков с настройками, которые указаны в yaml файле.  
Предназначен для автоматического создания топиков в ci/cd пайплайне.  

## Использование

Сначала формируем файл с названиями топиков, которые нужно создать.  
Для этого в отгружаемом сервисе есть команда, которая проверяет, что все используемые в коде топики созданы.  
Те которые не созданы будут выведены в stdout, их нужно сохранить в файл.
```bash
php artisan kafka:find-not-created-topics > unexisting-topics.txt
```
далее запускаем скрипт, передавая ему список топико и файл с параметрами топиков

```bash
docker run --rm -v $PWD:/wd -w /wd kafka-tools:latest \
  --bootstrap-server=127.0.0.1:9092 \
  --kafka-login=user --kafka-password=password \
  --topics-file=example/settings.yaml \
  --topic-names-file=example/unexisting-topics.txt
```

Если в файле `settings.yaml` есть настройки для топика, то он будет создан, иначе команду упадёт с ошибкой.  

## Файл настроек

Это файл в формате yaml, в котором можно указать количество реплик и партиций, а так же задать любые другие [параметры топика](https://kafka.apache.org/documentation/#topicconfigs).

```yaml
topics:
  - name: prod.all.fct.my-topic.0
    partitions: 2
    replicas: 1
    config:
      - name: retention.ms
        value: 60480000 # 7 days
      - name: retention.bytes
        value: 1073741824 # 1 Gb
```