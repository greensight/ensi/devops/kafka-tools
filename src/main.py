#!/usr/bin/python3

import re
import yaml
import argparse
import subprocess


dry_run = False

def read_config(config_file_path):
    with open(config_file_path, 'r') as f:
        topics_config = yaml.safe_load(f)

    for topic_options in topics_config["topics"]:
        topic_options["pattern"] = "^{}$".format(topic_options["name"].replace('*', '[^.]+'))

    return topics_config


def create_connection_config(login, password):
    with open('/tmp/config.properties', 'w+') as f:
        f.write(
            f"""sasl.jaas.config=org.apache.kafka.common.security.plain.PlainLoginModule required username="{login}" password="{password}";
security.protocol=SASL_PLAINTEXT
sasl.mechanism=PLAIN
""")


def create_topic(bootstrap_server, name, options):
    command = [
        'kafka-topics.sh',
        '--bootstrap-server', bootstrap_server,
        '--command-config', '/tmp/config.properties',
        '--create',
        '--if-not-exists',
        '--topic', name
    ]

    if options["partitions"]:
        command.extend(['--partitions=%s' % options["partitions"]])
    if options["replicas"]:
        command.extend(['--replication-factor=%s' % options["replicas"]])
    for item in options["config"]:
        key = item["name"]
        value = item["value"]
        command.extend(['--config', f"{key}={value}"])

    print('Run: %s' % ' '.join(command))

    if not dry_run:
        subprocess.run(command)


def read_names_from_file(filename):
    result = []
    with open(filename, 'r') as f:
        for line in f:
            name = line.strip()
            if name != "":
                result.append(name)

    return result


def get_all_topic_names(topic_configs_list):
    result = []
    for one_topic_config in topic_configs_list["topics"]:
        result.append(one_topic_config["name"])

    return result


def get_desired_topic_names(args, topics_config):
    result = []
    if args.topic_name is not None:
        if args.topic_name == 'all':
            result = get_all_topic_names(topics_config)
        else:
            result = [args.topic_name]
    elif args.topic_names_file is not None:
        result = read_names_from_file(args.topic_names_file)
    else:
        print("Either --topic-name or --topic-names-file option is required")
        exit(1)

    if len(result) == 0:
        print("No topics for create")
        exit(0)

    return result


def create_topics(desired_topic_names, topics_config):
    for topic_name in desired_topic_names:
        for topic_options in topics_config["topics"]:
            if not re.match(topic_options['pattern'], topic_name):
                continue
            create_topic(args.bootstrap_server, topic_name, topic_options)
            break



def check_all_configs_exists(desired_topic_names, topics_config):
    for desired_topic in desired_topic_names:
        config_found = False
        for topic_config in topics_config["topics"]:
            if re.match(topic_config['pattern'], desired_topic):
                config_found = True
                break
        if not config_found:
            print(f"No config for topic {desired_topic}")
            exit(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='create topics from file')
    parser.add_argument('--bootstrap-server', dest='bootstrap_server', required=True)
    parser.add_argument('--topics-file', dest='topics_file', required=True)
    parser.add_argument('--kafka-login', dest='kafka_login', required=True)
    parser.add_argument('--kafka-password', dest='kafka_password', required=True)
    parser.add_argument('--topic-name', dest='topic_name')
    parser.add_argument('--topic-names-file', dest='topic_names_file')
    parser.add_argument('--dry-run', dest='dry_run', action=argparse.BooleanOptionalAction)

    args = parser.parse_args()

    dry_run = args.dry_run

    topics_config = read_config(args.topics_file)
    desired_topic_names = get_desired_topic_names(args, topics_config)
    check_all_configs_exists(desired_topic_names, topics_config)
    create_connection_config(args.kafka_login, args.kafka_password)
    create_topics(desired_topic_names, topics_config)

    print('Done')
